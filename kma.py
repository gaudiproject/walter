import MeCab
import sys
import re

pattern = "^N|^V|^SL|^SN|^SH"

class KMA:
    def parse(self, keyword):
        # print(MeCab.VERSION)

        t = MeCab.Tagger(" ".join(sys.argv))
        # t.parse(query)

        keywords = ""
        m = t.parseToNode(keyword)
        while m:
            regex = re.compile(pattern)

            if re.match(regex, m.feature):
                keywords += m.feature.split(',')[3] + "/" + m.feature.split(',')[0] + " "

            m = m.next

        print(keywords)
        return keywords

    def parseQuery(self, keyword):
        keywords = self.parse(self, keyword).split(' ')

        return keywords
