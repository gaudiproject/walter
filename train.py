import os
import sys
import codecs
import logging
import multiprocessing
import gensim
from kma import KMA

SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))
TRAIN_PATH = os.path.join(SCRIPT_PATH, 'train/')
MODEL_PATH = os.path.join(SCRIPT_PATH, 'model/')

LOG_FILENAME = "logs/walter.log";
logger = logging.getLogger('train')

if __name__ == '__main__':
    fomatter = logging.Formatter('[%(asctime)s] [%(levelname)s] - %(message)s')
    fileHandler = logging.handlers.RotatingFileHandler(LOG_FILENAME, maxBytes=10000000, backupCount=5)
    streamHandler = logging.StreamHandler()
    fileHandler.setFormatter(fomatter)
    streamHandler.setFormatter(fomatter)

    logger.addHandler(fileHandler)
    logger.addHandler(streamHandler)
    logger.setLevel(logging.DEBUG)

    logger.debug("=========================================")
    logger.info("START")
    logger.info("TRAIN PATH : " + TRAIN_PATH)

    fileList = []

    for root, dirs, files in os.walk(TRAIN_PATH):
        for file in files:
            fileList.append(file)

    # if not os.path.isfile(TRAIN_PATH + 'data2.txt'):
    #     print('Train data has not found...')
    #     sys.exit()

    if len(fileList) == 0:
        logger.error('Train data has not found...')
        sys.exit()

    cores = multiprocessing.cpu_count()

    if not os.path.exists(MODEL_PATH):
        os.makedirs(MODEL_PATH)


    # Construct corpus using dictionary
    # wiki = gensim.corpora.WikiCorpus(WIKI_DUMP_FILEPATH, dictionary=dictionary)


    # class SentencesIterator:
    #     def __init__(self, wiki):
    #         self.wiki = wiki
    #
    #     def __iter__(self):
    #         for sentence in self.wiki.get_texts():
    #             yield list(map(lambda x: x.decode('utf-8'), sentence))

    # Initialize simple sentence iterator required for the Word2Vec model
    # sentences = SentencesIterator(wiki)

    class SentenceReader(object):
        def __init__(self, dirname):
            self.dirname = dirname

        def __iter__(self):
            for fname in os.listdir(self.dirname):
                for line in open(os.path.join(self.dirname, fname), encoding='utf-8'):
                    yield KMA.parse(KMA, line).split(' ')


    sentences = SentenceReader(TRAIN_PATH)

    logger.info('Training word2vec model...')
    model = gensim.models.Word2Vec(sentences=sentences, size=300, min_count=1, window=5, workers=cores)

    logger.info('Saving model...')
    model.save(os.path.join(MODEL_PATH, 'word2vec.model'))
    logger.info('Done training word2vec model!')

    logger.info("END!")


