
이 프로젝트는 Word2Vec을 활용해서 사용자가 원하는 맛집을 좀 더 효과적으로 찾는데 목적이 있습니다.

### 사전 설치 사항 ###

* Python3
> $ sudo apt install python3

* Pip3
> $ sudo apt install python3-pip

* MeCab
> $ sudo apt-get install libmecab-dev 
>
> $ sudo apt-get install mecab mecab-ipadic-utf8
>
> $ pip3 install mecab-python3

### 설치 ###

* [다운로드](https://bitbucket.org/gaudiproject/walter/downloads/) 후 압축 해제
* 실행 권한 변경
> $ chmod 755 *

* 설치
> $ ./setup.sh

### 학습 ###

> $ python3 train.py


### RestAPI 실행 ###

> $ python3 api.py

* 브라우저에서 확인 : http://localhost:9090/search/[keyword]