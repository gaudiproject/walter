# -*- coding: utf-8 -*-
# other required imports here

import os
import gensim
import json
import argparse
import logging
from flask import Flask, Response
from flask_restful import Resource, Api
from flask import jsonify
from kma import KMA
from logging.handlers import RotatingFileHandler

app = Flask(__name__)
api = Api(app)

LOG_FILENAME = "logs/walter.log";
SCRIPT_PATH = os.path.dirname(os.path.realpath(__file__))
MODEL_PATH  = os.path.join(SCRIPT_PATH, 'model/')

class Search(Resource):
    def get(self, query, headers=None):
        try:
            app.logger.info(query)

            # Load word2vec model
            model = gensim.models.Word2Vec.load(os.path.join(MODEL_PATH, 'word2vec.model'), mmap='r')

            results = []

            for word, sim in model.most_similar(KMA.parseQuery(KMA, query)):
                print('\"%s\"\t- similarity: %g' % (word, sim))

                result = {
                    'word': word,
                    'similar': sim
                }

                # append result to results
                results.append(result)

            print('')

            # resp = make_response(json.dumps(jsonify({'query': query, 'results': results}), ensure_ascii=False).decode('utf8'))
            # d = json.loads(resp.get_data())
            # d['time'] = float('%.4f' % (time.time() - tic))
            # resp.set_data(json.dumps(d, ensure_ascii=False).decode('utf8'))

            # response = make_response(json.dumps({'query': query, 'results': results}))
            # response.headers['Content-Type'] = 'application/json;charset=UTF-8'
            # response.mimetype = 'application/json;charset=utf-8'

            # return jsonify(results)

            # return make_response(json.dumps({"similar": "爱“, 'similar_norm': ”this－thing"}, ensure_ascii=False).decode('utf-8'))

            # data = jsonify({'query': query, 'results': results})
            data = json.dumps({'query': query, 'results': results})
            # type(data)  # unicode
            # return make_response(data)

            # resp = make_response(json.dumps(data), 'utf-8')
            # resp.headers.extend(headers or {})
            # return jsonify(resp.get_data())
            # j = {'d': '中', 'e': 'a'}

            # return json.dumps(j, ensure_ascii=False)

            # to_be_returned = make_response({'query': query, 'results': results})
            # to_be_returned.mimetype = 'application/json;charset=utf-8'
            # json_string = json.dumps(data, ensure_ascii=False)
            # creating a Response object to set the content type and the encoding
            json_string = json.dumps({'query': query, 'results': results}, ensure_ascii=False, indent=2)
            response = Response(json_string, content_type="application/json; charset=utf-8")

            return response
        except Exception as e:
            error = {
                'status': 'error',
                'msg': str(e)
            }

            app.logger.error(str(e))

            return jsonify(error)

api.add_resource(Search, '/search/<string:query>')

if __name__ == '__main__':
    # ----------- Parsing Arguments ---------------
    p = argparse.ArgumentParser()
    # p.add_argument("--model", help="Path to the trained model")
    # p.add_argument("--binary", help="Specifies the loaded model is binary")
    p.add_argument("--host", help="Host name (default: localhost)")
    p.add_argument("--port", help="Port (default: 5000)")
    # p.add_argument("--path", help="Path (default: /word2vec)")
    args = p.parse_args()

    host = args.host if args.host else "localhost"
    # path = args.path if args.path else "/word2vec"
    port = int(args.port) if args.port else 9090

    # if not args.model:
    #     print
    #     "Usage: api.py --model path/to/the/model [--host host --port 1234]"

    formatter = logging.Formatter("[%(asctime)s] [%(levelname)s] - %(message)s")
    handler = RotatingFileHandler(LOG_FILENAME, maxBytes=10000000, backupCount=5)
    # handler = RotatingFileHandler('walter.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)

    app.run(debug=True, host=host, port=port)
