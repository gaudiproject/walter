#!/bin/sh

# Log folder
mkdir logs

# Install required python libraries
pip3 install -r requirements.txt

# Run python script from training the model
python3 train.py

# Evaluate the model
python3 api.py